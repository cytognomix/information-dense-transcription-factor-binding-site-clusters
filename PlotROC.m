FVs = csvread('D:\Louie\PredictExprPattern\TPTNFVs.csv',1,1);
X = FVs(:,1:26149);
y = FVs(:,26150);
clear FVs;

SVMModel_RBF = fitcsvm(X,y,'KernelFunction','rbf');
SVMModel_RBF = fitPosterior(SVMModel_RBF);
[~,score_svm_RBF] = resubPredict(SVMModel_RBF);
[Xsvm_RBF,Ysvm_RBF,Tsvm_RBF,AUCsvm_RBF] = perfcurve(y,score_svm_RBF(:,2),'1');

SVMModel_Linear = fitcsvm(X,y,'KernelFunction','linear');
SVMModel_Linear = fitPosterior(SVMModel_Linear);
[~,score_svm_Linear] = resubPredict(SVMModel_Linear);
[Xsvm_Linear,Ysvm_Linear,Tsvm_Linear,AUCsvm_Linear] = perfcurve(y,score_svm_Linear(:,2),'1');

%SVMModel_Polynomial2 = fitcsvm(X,y,'KernelFunction','polynomial','PolynomialOrder',2);
%SVMModel_Polynomial2 = fitPosterior(SVMModel_Polynomial2);
%[~,score_svm_Polynomial2] = resubPredict(SVMModel_Polynomial2);
%[Xsvm_Polynomial2,Ysvm_Polynomial2,Tsvm_Polynomial2,AUCsvm_Polynomial2] = perfcurve(y,score_svm_Polynomial2(:,2),'1');

%SVMModel_Polynomial3 = fitcsvm(X,y,'KernelFunction','polynomial','PolynomialOrder',3);
%SVMModel_Polynomial3 = fitPosterior(SVMModel_Polynomial3);
%[~,score_svm_Polynomial3] = resubPredict(SVMModel_Polynomial3);
%[Xsvm_Polynomial3,Ysvm_Polynomial3,Tsvm_Polynomial3,AUCsvm_Polynomial3] = perfcurve(y,score_svm_Polynomial3(:,2),'1');

TreeModel = fitctree(X,y);
[~,score_Tree] = resubPredict(TreeModel);
[Xtree,Ytree,Ttree,AUCtree] = perfcurve(y,score_Tree(:,2),'1');

Forest100Model = TreeBagger(100,X,y,'OOBPrediction','on');
[~,score_Forest100] = oobPredict(Forest100Model);
[Xforest100,Yforest100,Tforest100,AUCforest100] = perfcurve(y,score_Forest100(:,2),'1');

BayesModel = fitcnb(X,y,'DistributionNames','kernel');
[~,score_NB] = resubPredict(BayesModel);
[Xnb,Ynb,Tnb,AUCnb] = perfcurve(y,score_NB(:,2),'1');

plot(Xsvm_RBF,Ysvm_RBF)
hold on
plot(Xsvm_Linear,Ysvm_Linear)
plot(Xsvm_Polynomial2,Ysvm_Polynomial2)
plot(Xsvm_Polynomial3,Ysvm_Polynomial3)
plot(Xtree,Ytree)
plot(Xforest100,Yforest100)
plot(Xnb,Ynb)

set(gcf,'Position',[300 300 350 350]);
xlabel('False positive rate');
ylabel('True positive rate');
%legend('SVM(RBF):0.9941','SVM(Linear):0.6460','SVM(Polynomial2):0.8701','SVM(Polynomial3):0.7780','Decison Tree:0.9987','Random Forest:0.8677','Naive Bayes:0.9969','Location','Best');
%export_fig ROC_NR3C1_EP.tiff -r600